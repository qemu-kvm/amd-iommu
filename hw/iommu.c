/*
 * Generic IOMMU layer
 *
 * Copyright (c) 2010 Eduard - Gabriel Munteanu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <errno.h>

#include "iommu.h"

struct iommu *iommu_get(DeviceState *dev, DeviceState **real_dev)
{
    BusState *bus;

    while (dev) {
        bus = dev->parent_bus;
        if (!bus)
            goto out;

        if (bus->iommu) {
            *real_dev = dev;
            return bus->iommu;
        }

        dev = bus->parent;
    }

out:
    *real_dev = NULL;
    return NULL;
}

int __iommu_rw(struct iommu *iommu,
               DeviceState *dev,
               target_phys_addr_t addr,
               uint8_t *buf,
               int len,
               int is_write)
{
    int plen, err;
    target_phys_addr_t paddr;
    unsigned perms;

    if (!is_write)
        perms = IOMMU_PERM_READ;
    else
        perms = IOMMU_PERM_WRITE;

    do {
        err = iommu->translate(iommu, dev, addr, &paddr, &plen, perms);
        if (err)
            return err;
        if (plen > len)
            plen = len;

        cpu_physical_memory_rw(paddr, buf, plen, is_write);

        len -= plen;
        addr += plen;
        buf += plen;
    } while (len);

    return 0;
}
