#ifndef QEMU_IOMMU_H
#define QEMU_IOMMU_H

#include "pci.h"
#include "targphys.h"
#include "qdev.h"

/* Don't use directly. */
struct iommu {
    void *opaque;

    void (*register_device)(struct iommu *iommu,
                            DeviceState *dev);
    int (*translate)(struct iommu *iommu,
                     DeviceState *dev,
                     target_phys_addr_t addr,
                     target_phys_addr_t *paddr,
                     int *len,
                     unsigned perms);
    int (*start_transaction)(struct iommu *iommu,
                             DeviceState *dev);
    void (*end_transaction)(struct iommu *iommu,
                            DeviceState *dev);
};

#define IOMMU_PERM_READ   (1 << 0)
#define IOMMU_PERM_WRITE  (1 << 1)

#define IOMMU_PERM_RW     (IOMMU_PERM_READ | IOMMU_PERM_WRITE)

static inline int iommu_nop_translate(struct iommu *iommu,
                                      DeviceState *dev,
                                      target_phys_addr_t addr,
                                      target_phys_addr_t *paddr,
                                      int *len,
                                      unsigned perms)
{
    *paddr = addr;
    *len = INT_MAX;

    return 0;
}

static inline int iommu_nop_rw(struct iommu *iommu,
                               DeviceState *dev,
                               target_phys_addr_t addr,
                               uint8_t *buf,
                               int len,
                               int is_write)
{
    cpu_physical_memory_rw(addr, buf, len, is_write);

    return 0;
}

static inline int iommu_register_device(struct iommu *iommu,
                                        DeviceState *dev)
{
    if (iommu && iommu->register_device)
        iommu->register_device(iommu, dev);

    return 0;
}

#ifdef CONFIG_IOMMU

extern struct iommu *iommu_get(DeviceState *dev, DeviceState **real_dev);

/**
 * Translates an address for the given device and performs access checking.
 *
 * Defined in implementation-specific IOMMU code.
 *
 * @iommu   IOMMU
 * @dev     qdev device
 * @addr    address to be translated
 * @paddr   translated address
 * @len     number of bytes for which the translation is valid
 * @rw      read or write?
 *
 * Returns 0 iff translation and access checking succeeded.
 */
static inline int iommu_translate(struct iommu *iommu,
                                  DeviceState *dev,
                                  target_phys_addr_t addr,
                                  target_phys_addr_t *paddr,
                                  int *len,
                                  unsigned perms)
{
    if (iommu && iommu->translate)
        return iommu->translate(iommu, dev, addr, paddr, len, perms);

    return iommu_nop_translate(iommu, dev, addr, paddr, len, perms);
}

extern int __iommu_rw(struct iommu *iommu,
                      DeviceState *dev,
                      target_phys_addr_t addr,
                      uint8_t *buf,
                      int len,
                      int is_write);

/**
 * Performs I/O with address translation and access checking.
 *
 * Defined in generic IOMMU code.
 *
 * @iommu   IOMMU
 * @dev     qdev device
 * @addr    address where to perform I/O
 * @buf     buffer to read from or write to
 * @len     length of the operation
 * @rw      read or write?
 *
 * Returns 0 iff the I/O operation succeeded.
 */
static inline int iommu_rw(struct iommu *iommu,
                           DeviceState *dev,
                           target_phys_addr_t addr,
                           uint8_t *buf,
                           int len,
                           int is_write)
{
    if (iommu && iommu->translate)
        return __iommu_rw(iommu, dev, addr, buf, len, is_write);

    return iommu_nop_rw(iommu, dev, addr, buf, len, is_write);
}

static inline int iommu_start_transaction(struct iommu *iommu,
                                          DeviceState *dev)
{
    if (iommu && iommu->start_transaction)
        return iommu->start_transaction(iommu, dev);

    return 0;
}

static inline void iommu_end_transaction(struct iommu *iommu,
                                         DeviceState *dev)
{
    if (iommu && iommu->end_transaction)
        iommu->end_transaction(iommu, dev);
}

#define DEFINE_LD_PHYS(suffix, size)                                        \
static inline uint##size##_t iommu_ld##suffix(struct iommu *iommu,          \
                                             DeviceState *dev,              \
                                             target_phys_addr_t addr)       \
{                                                                           \
    int len, err;                                                           \
    target_phys_addr_t paddr;                                               \
                                                                            \
    err = iommu_translate(iommu, dev, addr, &paddr, &len, IOMMU_PERM_READ); \
    if (err || (len < size / 8))                                            \
        return err;                                                         \
    return ld##suffix##_phys(paddr);                                        \
}

#define DEFINE_ST_PHYS(suffix, size)                                        \
static inline void iommu_st##suffix(struct iommu *iommu,                    \
                                    DeviceState *dev,                       \
                                    target_phys_addr_t addr,                \
                                    uint##size##_t val)                     \
{                                                                           \
    int len, err;                                                           \
    target_phys_addr_t paddr;                                               \
                                                                            \
    err = iommu_translate(iommu, dev, addr, &paddr, &len, IOMMU_PERM_WRITE);\
    if (err || (len < size / 8))                                            \
        return;                                                             \
    st##suffix##_phys(paddr, val);                                          \
}

#else /* CONFIG_IOMMU */

static inline struct iommu *iommu_get(DeviceState *dev, DeviceState **real_dev)
{
    return NULL;
}

static inline int iommu_translate(struct iommu *iommu,
                                  DeviceState *dev,
                                  target_phys_addr_t addr,
                                  target_phys_addr_t *paddr,
                                  int *len,
                                  unsigned perms)
{
    return iommu_nop_translate(iommu, dev, addr, paddr, len, perms);
}

static inline int iommu_rw(struct iommu *iommu,
                           DeviceState *dev,
                           target_phys_addr_t addr,
                           uint8_t *buf,
                           int len,
                           int is_write)
{
    return iommu_nop_rw(iommu, dev, addr, buf, len, is_write);
}

static inline int iommu_start_transaction(struct iommu *iommu,
                                          DeviceState *dev)
{
    return 0;
}

static inline void iommu_end_transaction(struct iommu *iommu,
                                         DeviceState *dev)
{
}

#define DEFINE_LD_PHYS(suffix, size)                                        \
static inline uint##size##_t iommu_ld##suffix(struct iommu *iommu,          \
                                             DeviceState *dev,              \
                                             target_phys_addr_t addr)       \
{                                                                           \
    return ld##suffix##_phys(addr);                                         \
}

#define DEFINE_ST_PHYS(suffix, size)                                        \
static inline void iommu_st##suffix(struct iommu *iommu,                    \
                                    DeviceState *dev,                       \
                                    target_phys_addr_t addr,                \
                                    uint##size##_t val)                     \
{                                                                           \
    st##suffix##_phys(addr, val);                                           \
}

#endif /* CONFIG_IOMMU */

static inline int iommu_read(struct iommu *iommu,
                             DeviceState *dev,
                             target_phys_addr_t addr,
                             uint8_t *buf,
                             int len)
{
    return iommu_rw(iommu, dev, addr, buf, len, 0);
}

static inline int iommu_write(struct iommu *iommu,
                              DeviceState *dev,
                              target_phys_addr_t addr,
                              const uint8_t *buf,
                              int len)
{
    return iommu_rw(iommu, dev, addr, (uint8_t *) buf, len, 1);
}

DEFINE_LD_PHYS(ub, 8)
DEFINE_LD_PHYS(uw, 16)
DEFINE_LD_PHYS(l, 32)
DEFINE_LD_PHYS(q, 64)

DEFINE_ST_PHYS(b, 8)
DEFINE_ST_PHYS(w, 16)
DEFINE_ST_PHYS(l, 32)
DEFINE_ST_PHYS(q, 64)

#endif
